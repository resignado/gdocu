-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2018 a las 11:48:59
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestordocumental`
--
CREATE DATABASE IF NOT EXISTS `gestordocumental` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `gestordocumental`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elemento_clientes`
--

DROP TABLE IF EXISTS `elemento_clientes`;
CREATE TABLE `elemento_clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` timestamp NULL DEFAULT NULL,
  `id_creador` int(11) NOT NULL,
  `id_ultimo_modificador` int(11) DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poblacion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigopostal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `movil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notas` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `elemento_clientes`
--

INSERT INTO `elemento_clientes` (`id`, `fecha_creacion`, `fecha_ultima_modificacion`, `id_creador`, `id_ultimo_modificador`, `borrado`, `nombre`, `apellidos`, `nif`, `direccion`, `poblacion`, `codigopostal`, `telefono`, `movil`, `email`, `notas`) VALUES
(4, '2018-05-26 18:23:46', NULL, 1, NULL, 0, 'Jose Luis', 'Ferreira', '45896521L', 'Elviña S/N', 'A Coruña', '15003', '981253654', '665894521', 'jferreira@gmail.com', NULL),
(5, '2018-05-26 18:23:46', NULL, 1, NULL, 0, 'Alejandro', 'Garcia Sanchez', '53169726M', 'C/ Ramon del cueto num 23 5ºA', 'A Coruña', '15002', '981266905', '662552082', 'alexparrillero@gmail.com', NULL),
(6, '2018-05-26 18:23:46', NULL, 1, NULL, 0, 'Oscar', 'Aviles', '45896547M', 'Barcelona num 36 3º', 'A Coruña', '15010', '981252365', '645269871', 'oaviles@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elemento_documentos`
--

DROP TABLE IF EXISTS `elemento_documentos`;
CREATE TABLE `elemento_documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` timestamp NULL DEFAULT NULL,
  `id_creador` int(11) NOT NULL,
  `id_ultimo_modificador` int(11) NOT NULL,
  `borrado` tinyint(1) NOT NULL,
  `nombreoriginal` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_publicacion` timestamp NULL DEFAULT NULL,
  `tamano` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notas` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `elemento_usuarios`
--

DROP TABLE IF EXISTS `elemento_usuarios`;
CREATE TABLE `elemento_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` timestamp NULL DEFAULT NULL,
  `borrado` tinyint(1) NOT NULL,
  `usuario` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmado` tinyint(1) NOT NULL DEFAULT '0',
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `es_administrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `elemento_usuarios`
--

INSERT INTO `elemento_usuarios` (`id`, `fecha_creacion`, `fecha_ultima_modificacion`, `borrado`, `usuario`, `password`, `nombre`, `apellidos`, `direccion`, `email`, `confirmado`, `telefono`, `movil`, `rol`, `es_administrador`) VALUES
(4, '2018-05-26 11:15:07', NULL, 0, 'root', 'kusanagui', 'Alejandro', 'Garcia Sanchez', 'C/ Ramon del Cueto num 23 5A', 'alexparrillero@gmail.com', 0, '98190609', '662552082', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(52, '2018_05_25_171108_create_elemento_usuarios_table', 1),
(53, '2018_05_25_174051_add_votes_to_elemento_usuarios_table', 1),
(54, '2018_05_26_101102_create_elemento_clientes_table', 1),
(55, '2018_05_26_102313_create_elemento_documentos_table', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `elemento_clientes`
--
ALTER TABLE `elemento_clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elemento_clientes_nombre_index` (`nombre`);

--
-- Indices de la tabla `elemento_documentos`
--
ALTER TABLE `elemento_documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elemento_documentos_nombreoriginal_index` (`nombreoriginal`),
  ADD KEY `elemento_documentos_nombre_index` (`nombre`);

--
-- Indices de la tabla `elemento_usuarios`
--
ALTER TABLE `elemento_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elemento_usuarios_usuario_unique` (`usuario`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `elemento_clientes`
--
ALTER TABLE `elemento_clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `elemento_documentos`
--
ALTER TABLE `elemento_documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `elemento_usuarios`
--
ALTER TABLE `elemento_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
