<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementoDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elemento_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_ultima_modificacion')->nullable();
            $table->integer('id_creador');
            $table->integer('id_ultimo_modificador')->nullable();;
            $table->boolean('borrado');
            $table->string('nombreoriginal',100)->index();
            $table->string('nombre',100)->index();
            $table->timestamp('fecha_publicacion')->nullable();
            $table->string('tamano');
            $table->string('mime');
            $table->text('notas')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elemento_documentos');
    }
}
