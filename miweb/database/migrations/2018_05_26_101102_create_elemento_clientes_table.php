<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementoClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elemento_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha_creacion');
            $table->timestamp('fecha_ultima_modificacion')->nullable();
            $table->integer('id_creador');
            $table->integer('id_ultimo_modificador')->nullable();
            $table->boolean('borrado');
            $table->string('nombre', 32)->index(); //Indexado
            $table->string('apellidos')->nullable();
            $table->string('nif')->nullable();
            $table->string('direccion')->nullable();
            $table->string('poblacion')->nullable();
            $table->string('codigopostal')->nullable();
            $table->string('telefono')->nullable();
            $table->string('movil')->nullable();
            $table->string('email')->nullable();          
            $table->text('notas')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elemento_clientes');
    }
}
