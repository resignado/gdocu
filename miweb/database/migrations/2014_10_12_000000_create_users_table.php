<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',150)->unique();
            $table->string('password')->nullable();
            $table->boolean('borrado');
            $table->string('nombre', 32)->nullable();
            $table->string('apellidos')->nullable();
            $table->string('direccion')->nullable();
            $table->boolean('confirmado')->default(false);
            $table->string('telefono')->nullable();
            $table->string('movil')->nullable();
            $table->integer('rol');
            $table->integer('es_administrador');
            $table->rememberToken();
            $table->timestamps();
            $table->integer('id_cliente')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
