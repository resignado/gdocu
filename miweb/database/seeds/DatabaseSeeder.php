<?php

use Illuminate\Database\Seeder;
use app\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(elemento_clientesTableSeeder::class);
        $this->command->info('Tabla clientes inicializada con datos!');
        self::seedUsers();
        $this->command->info('Tabla usuarios inicializada con datos!');
    }
    
    public function seedUsers(){
        DB::table('users')->delete();
        $user = new App\Models\User();
        $user->password   = bcrypt('kusanagui');
        $user->nombre     = 'Alejandro';
        $user->apellidos  = 'Garcia Sanchez';
        $user->direccion  = 'C/ Ramon del Cueto num 23 5A';
        $user->email      = 'alexparrillero@gmail.com';
        $user->telefono   = '98190609';
        $user->movil      = '662552082';
        $user->rol        = 1;
        $user->borrado    = 0;
        $user->es_administrador = 1;            
        $user->save();
    }
}
