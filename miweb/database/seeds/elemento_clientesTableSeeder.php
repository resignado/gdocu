<?php

use Illuminate\Database\Seeder;

class elemento_clientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elemento_clientes')->delete();
        //Array de clientes por defecto
        $clientes = array();
        $clientes[] = array(                            
                            "nombre" => "Jose Luis",
                            "id_creador" => 1,
                            "apellidos" => "Ferreira",
                            "nif" => "45896521L",
                            "direccion" => "Elviña S/N",
                            "poblacion" => "A Coruña",
                            "codigopostal" => "15003",
                            "telefono" => "981253654",
                            "movil" => "665894521",
                            "email" => "jferreira@gmail.com",
                            "borrado" => 0  
                );
        $clientes[] = array(                            
                            "nombre" => "Alejandro",
                            "id_creador" => 1,
                            "apellidos" => "Garcia Sanchez",
                            "nif" => "53169726M",
                            "direccion" => "C/ Ramon del cueto num 23 5ºA",
                            "poblacion" => "A Coruña",
                            "codigopostal" => "15002",
                            "telefono" => "981266905",
                            "movil" => "662552082",
                            "email" => "alexparrillero@gmail.com",
                            "borrado" => 0
                );
        $clientes[] = array(                            
                            "nombre" => "Oscar",
                            "id_creador" => 1,
                            "apellidos" => "Aviles",
                            "nif" => "45896547M",
                            "direccion" => "Barcelona num 36 3º",
                            "poblacion" => "A Coruña",
                            "codigopostal" => "15010",
                            "telefono" => "981252365",
                            "movil" => "645269871",
                            "email" => "oaviles@gmail.com",
                            "borrado" => 0  
                );
        
        foreach( $clientes as $cliente ) {
            $obj_cliente = new App\Models\Clientes;
            $obj_cliente->id_creador = $cliente['id_creador'];
            $obj_cliente->nombre = $cliente['nombre'];
            $obj_cliente->apellidos = $cliente['apellidos'];
            $obj_cliente->direccion = $cliente['direccion'];
            $obj_cliente->poblacion = $cliente['poblacion'];
            $obj_cliente->nif = $cliente['nif'];
            $obj_cliente->codigopostal = $cliente['codigopostal'];
            $obj_cliente->telefono = $cliente['telefono'];
            $obj_cliente->movil = $cliente['movil'];
            $obj_cliente->email = $cliente['email'];
            $obj_cliente->borrado = $cliente['borrado'];
            $obj_cliente->save();
        }
    }
}
