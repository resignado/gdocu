<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Documentos extends Model
{
    protected $table = 'elemento_documentos';
    //Si queremos cambiar la clave primaria de la tabla
    //protected $primaryKey = 'my_id';
    //No queremos usar el create_at y updated_at
    public $timestamps = false;
    
    public function get_documentos_cliente($id_cliente){
        $obj_documentos = new Documentos();
        $documentos = $obj_documentos->where('id_cliente',$id_cliente)->where('borrado',0)->get();
        return $documentos;
    }
    
    public function get_documentos_cliente_online($id_cliente){
        $obj_documentos = new Documentos();
        $documentos = $obj_documentos->where('id_cliente',$id_cliente)->where('borrado',0)->where('online',1)->get();
        return $documentos;
    }
}
