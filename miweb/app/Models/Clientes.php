<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $table = 'elemento_clientes';
    //Si queremos cambiar la clave primaria de la tabla
    //protected $primaryKey = 'my_id';
    //No queremos usar el create_at y updated_at
    public $timestamps = false;
    
}
