<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
/*ROLES
 * 1 - Administrador
 * 2 - Usuario 
 * 3 - Usuario Online
 */
class UserController extends Controller
{
    public function getlist(){
        $usuarios_todos = User::all()->where("borrado",0);
        $usuarios = array();
        $usuarios_online = Array();
        foreach($usuarios_todos as $usuario){
            if ($usuario->rol == 1){
              $usuarios[$usuario->id] = $usuario;
            } else {
              $usuarios_online[$usuario->id] = $usuario;  
            }
        }
        
        return view('users.list',array('usuarios'=>$usuarios,'usuarios_online'=>$usuarios_online));
    }
    
    public function getCreate($id,$id_cliente = '')
    {
        return view('users.create',array("rol" => $id,"id_cliente" => $id_cliente));
    }
    
    public function saveUser(Request $request){
        $usuario     = $request->all();  
        
        $user = new User();
        $user->email      = $usuario['email'];
        $user->password   = bcrypt($usuario['password']);
        $user->nombre     = $usuario['nombre'];
        $user->apellidos  = $usuario['apellidos'];
        $user->direccion  = $usuario['direccion'];
        $user->telefono   = $usuario['telefono'];
        $user->movil      = $usuario['movil'];
        $user->rol        = $usuario['rol'];
        $user->borrado    = 0;
        if (isset($usuario['administrador']) && $usuario['administrador'] == 'on'){
            $user->es_administrador = 1;            
        }else {
            $user->es_administrador = 0;            
        }
        if (isset($usuario['id_cliente']) && $usuario['id_cliente']){
            $user->id_cliente = $usuario['id_cliente'];            
        }else {
            $user->id_cliente = 0;            
        }
        $user->save();
        if (isset($usuario['id_cliente']) && $usuario['id_cliente']){
            return redirect()->action('ClientesController@getShow', $usuario['id_cliente']);
        } else {
            return redirect()->action('UserController@getShow', $user->id);
        }    
    }
    
    public function getShow($id)
    {
        $usuario = User::find($id);
        
        return view('users.show',array('rol' => $usuario->rol,'datosusuario'=>$usuario));
    }
    
    public function getDelete($id,$id_cliente = '')
    {
        $usuario = User::find($id);
        $usuario->borrado = 1;
        $usuario->save();
        if ($id_cliente){
            return redirect()->action('ClientesController@getShow', $id_cliente);
        } else {
            return redirect()->action('UserController@getList');
        }    
    }
    
    public function getEdit($id,$id_cliente = '')
    {
        $usuario = User::find($id);
        return view('users.edit',array('rol' => $usuario->rol,'datosusuario'=>$usuario,"id_cliente" => $id_cliente));
    }
    
    public function saveeditUser(Request $request){
        $usuario     = $request->all(); 
        $obj_usuario = User::find($usuario['id_user']);
        $obj_usuario->nombre = $usuario['nombre'];
        $obj_usuario->apellidos = $usuario['apellidos'];
        $obj_usuario->direccion = $usuario['direccion'];
        $obj_usuario->telefono = $usuario['telefono'];
        $obj_usuario->movil = $usuario['movil'];
        $obj_usuario->email = $usuario['email'];
        if($usuario['password'] != ''){
            $obj_usuario->password   = bcrypt($usuario['password']);
        }
        if (isset($usuario['administrador']) && $usuario['administrador'] == 'on'){
            $obj_usuario->es_administrador = 1;
        } else {
            $obj_usuario->es_administrador = 0;
        }   
        $obj_usuario->borrado = 0;
        $obj_usuario->save();
        if (isset($usuario['id_cliente']) && $usuario['id_cliente']){
            return redirect()->action('ClientesController@getShow', $usuario['id_cliente']);
        } else {
            return redirect()->action('UserController@getShow', $obj_usuario->id);
        }  
    }
}
