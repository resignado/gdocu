<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Documentos;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Illuminate\Support\Facades\Auth;

use Storage;

class DocumentosController extends Controller
{
    public function getCreate($id_cliente)
    {
       return view('documentos.create',array('cliente'=>$id_cliente));
    }
    
    public function saveDocumento(Request $request){
        $documento     = $request->all(); 
        $obj_documento = new Documentos();
        $obj_documento->id_creador = Auth::user()->id;
        $obj_documento->nombre = $documento['nombre'];
        if (isset($documento['online']) && $documento['online'] == 'on'){
            $obj_documento->online = 1;
        } else {
            $obj_documento->online = 0;
        }    
        $obj_documento->fecha_publicacion = date('Y-m-d H:i:s',strtotime($documento['fecha_publicacion']));
        $obj_documento->mime = $request->fichero->getmimeType();
        $obj_documento->id_cliente = $documento['id_cliente'];
        $obj_documento->nombreoriginal = $request->fichero->getClientOriginalName();
        $obj_documento->tamano = $request->fichero->getsize();
        $obj_documento->notas = $documento['notas'];
        
        $ruta_doc = 'gestordocumental/'.$obj_documento->id_cliente;
        $obj_documento->ruta = $ruta_doc;
        $obj_documento->borrado = 0;
        $obj_documento->save();
        
        $nombre_doc = $obj_documento->id.".dat";
        $request->fichero->storeAs($ruta_doc,$nombre_doc,'s3');
        if (Auth::user()->rol == 2){
            return redirect('clientes/showdocumentos/cliente/'.$obj_documento->id_cliente);
        } else {
            return redirect()->action('ClientesController@getShow', $obj_documento->id_cliente);
        }    
    }
    public function getDelete($id)
    {
        $documento = Documentos::find($id);
        $documento->borrado = 1;
        $documento->save();
        return redirect()->action('ClientesController@getShow', $documento->id_cliente);
    }
    public function getEdit($id,$id_cliente)
    {
        $documento = Documentos::find($id);
        $documento->fecha_publicacion = date('Y-m-d',strtotime($documento->fecha_publicacion));
        $cliente = array('id_cliente' => $id_cliente);
        return view('documentos.edit',array('datosdocumento'=>$documento,'cliente' => $cliente));
    }
    public function saveeditDocumento(Request $request){
        $documento     = $request->all(); 
        $obj_documento = Documentos::find($documento['id_documento']);;
        $obj_documento->id_ultimo_modificador = Auth::user()->id;
        $obj_documento->fecha_ultima_modificacion = date('Y-m-d H:i:s');
        $obj_documento->fecha_publicacion = date('Y-m-d H:i:s',strtotime($documento['fecha_publicacion']));
        $obj_documento->nombre = $documento['nombre'];
        if (isset($documento['online']) && $documento['online'] == 'on'){
            $obj_documento->online = 1;
        } else {
            $obj_documento->online = 0;
        }    
        
        $obj_documento->notas = $documento['notas'];        
        
        $obj_documento->save();
        return redirect()->action('ClientesController@getShow', $documento['id_cliente']);
    }
    
    public function getShow($id)
    {
        $documento = Documentos::find($id);
        $documento->fecha_publicacion = date('Y-m-d',strtotime($documento->fecha_publicacion));
        return view('documentos.show',array('datosdocumento'=>$documento));
    }
    
    public function descargaficheros3($id) {
        $documento  = Documentos::find($id);
        $key        = 'gestordocumental/'.$documento->id_cliente.'/'.$documento->id.'.dat';
        $disposition= 'attachment';
        $expiration = '+300 seconds';
        $disk = Storage::disk('s3');
        $cliente_s3 = $disk->getDriver()->getAdapter()->getClient();
        
        $nombre_fichero = utf8_encode($documento->nombreoriginal);
        $params = array(
                    'Bucket' => 'proyecto-fct',
                    'Key'    => $key, 
                    'ResponseContentDisposition' => $disposition.'; filename="'.$nombre_fichero.'"'
                );
        $cmd = $cliente_s3->getCommand('GetObject', $params);
        $enlace = (string) $cliente_s3->createPresignedRequest($cmd, $expiration)->getUri(); //con expiration time
        header('Location: '.$enlace);
        die();
    }	
}
