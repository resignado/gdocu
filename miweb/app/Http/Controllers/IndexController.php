<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    
    public function getHome()
    {
        //return "hola";
        return redirect()->action('ClientesController@getIndex');
    }
}
