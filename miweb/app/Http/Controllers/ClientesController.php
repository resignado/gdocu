<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Clientes;
use App\Models\Documentos;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class ClientesController extends Controller
{   
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function getcliente($id)
    {
        return view('clientes.show', array('id'=>$id));
    }
    public function editcliente($id)
    {
        return view('clientes.edit', array('id'=>$id));
    }
    
    public function getIndex()
    {
        //Solo se muestran clientes no borrados
        $clientes = Clientes::all()->where("borrado",0);
        return view('clientes.index',array('clientes'=>$clientes));
    }
    
    public function getShow($id)
    {
        $cliente = Clientes::find($id);
        $obj_documentos = new Documentos();
        $documentos =$obj_documentos->get_documentos_cliente($id);
        //return view('clientes.show',array('datoscliente'=>$cliente));
        $usuario_online = User::all()->where("id_cliente",$id)->where('borrado',0);
        return view('clientes.show',array('datoscliente'=>$cliente,'documentos'=>$documentos,'usuario_online'=>$usuario_online));
    }
    public function getCreate()
    {
        return view('clientes.create');
    }
    public function getEdit($id)
    {
        $cliente = Clientes::find($id);
        return view('clientes.edit',array('datoscliente'=>$cliente));
    }
    public function getDelete($id)
    {
        $cliente = Clientes::find($id);
        $cliente->borrado = 1;
        $cliente->save();
        return redirect()->action('ClientesController@getIndex');
    }
    
    public function saveCliente(Request $request){
        $cliente     = $request->all();    
        $obj_cliente = new Clientes();
        $obj_cliente->id_creador = Auth::user()->id;
        $obj_cliente->nombre = $cliente['nombre'];
        $obj_cliente->apellidos = $cliente['apellidos'];
        $obj_cliente->direccion = $cliente['direccion'];
        $obj_cliente->poblacion = $cliente['poblacion'];
        $obj_cliente->nif = $cliente['nif'];
        $obj_cliente->codigopostal = $cliente['codigopostal'];
        $obj_cliente->telefono = $cliente['telefono'];
        $obj_cliente->movil = $cliente['movil'];
        $obj_cliente->email = $cliente['email'];
        $obj_cliente->notas = $cliente['notas'];
        $obj_cliente->borrado = 0;
        $obj_cliente->save();
        return redirect()->action('ClientesController@getShow', $obj_cliente->id);
    }
    
    public function saveeditCliente(Request $request){
        $cliente     = $request->all();  
        $obj_cliente = Clientes::find($cliente['id_cliente']);
        $obj_cliente->id_ultimo_modificador = Auth::user()->id;
        $obj_cliente->fecha_ultima_modificacion = date('Y-m-d H:i:s');
        $obj_cliente->nombre = $cliente['nombre'];
        $obj_cliente->apellidos = $cliente['apellidos'];
        $obj_cliente->direccion = $cliente['direccion'];
        $obj_cliente->poblacion = $cliente['poblacion'];
        $obj_cliente->nif = $cliente['nif'];
        $obj_cliente->codigopostal = $cliente['codigopostal'];
        $obj_cliente->telefono = $cliente['telefono'];
        $obj_cliente->movil = $cliente['movil'];
        $obj_cliente->email = $cliente['email'];
        $obj_cliente->notas = $cliente['notas'];
        $obj_cliente->borrado = 0;
        $obj_cliente->save();
        return redirect()->action('ClientesController@getShow', $obj_cliente->id);
    }
    
    public function getDocumentoscliente($id_cliente){
        $cliente = Clientes::find($id_cliente);
        $obj_documentos = new Documentos();
        $documentos =$obj_documentos->get_documentos_cliente_online($id_cliente);
        return view('clientes.showdocumentos',array('datoscliente'=>$cliente,'documentos'=>$documentos));
    }
}
