<?php

namespace App\Http\Middleware;

use Closure;

class RolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  strin \rol     $rol
     * @return mixed
     */
    public function handle($request, Closure $next, $rol)
    {
        if (! $request->user()->tieneRol($rol)) {
        // No tiene el rol esperado!
        }
        return $next($request);
        }
}
