<?php

namespace App\Http\Middleware;

use Closure;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    //Antes de la peticion
    public function handle($request, Closure $next)
    {
        //Si es menos de 18 años redirige a home
        if ($request->input('age') < 18) {
            //abort(403, 'Unauthorized action.');
            return redirect('/');
        }
        return $next($request);
    }
    /*
    //Despues de la peticion
    public function handle($request, Closure $next)
    {
    $response = $next($request);
    // Código a ejecutar después de la petición
    return $response;
    }
     */
}
