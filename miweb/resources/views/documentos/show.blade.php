@extends('layouts.master')
    @section('content')
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos del Documento				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_datos_personales" style="display:" class="row">
		<div class="col-sm-6 form-horizontal">
                    <div class="col-sm-12 form-horizontal">
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_nombre">{{$datosdocumento['nombre']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Online&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                @if($datosdocumento['online'] == 1)
                                    <i class="fa fa-check green"></i>
                                @else
                                    <i class="fa fa-close red"></i>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Fecha Publicaci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_nombre">{{$datosdocumento['fecha_publicacion']}}</span>
                            </div>
                        </div>
                    </div>
		</div>
            </div>
	</div>
    </div>
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Notas				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_notas" style="display:" class="row">
		<div class="form-group form-custom hidden-xs">
                    <label class="control-label col-sm-1 nombredato" style="text-align:left !important;"><b>Notas&nbsp;</b></label>
                    <div class="col-sm-11 cssdefault">
                        <span class="form-control-static" id="view_notas">{{$datosdocumento['notas']}}</span>
                    </div>
                </div>
            </div>
	</div>
    </div>
    @stop
    
    
