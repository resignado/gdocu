@extends('layouts.master')
    @section('content')
    <form action="{{ url('documentos/save') }}" enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id_cliente" id="id_cliente" value="{{$cliente}}">
        <div class="portlet light bordered portlet-custom clearfix">
            <div class="portlet-title">
                <div class="caption" style="cursor:pointer;">
                    <i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos Personales				
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display:block;">
                <div id="panel_datos_personales" style="display:" class="row">
                    <div class="col-sm-12 form-horizontal">
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Seleccione el documento&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="fichero" id="fichero" class="normal" type="file">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="nombre" id="nombre" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Online&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="online" id="online" type="checkbox">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Fecha Publicaci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <div class="input-icon input-icon-sm inline">
                                    <i class="fa fa-calendar"></i>
                                    <input class="form-control input-small datepicker" name="fecha_publicacion" id="fecha_publicacion" value="<?=date('Y-m-d');?>" maxlength="10" size="12" tabindex="-1" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet light bordered portlet-custom clearfix">
            <div class="portlet-title">
                <div class="caption" style="cursor:pointer;">
                    <i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Notas				
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display:block;">
                <div id="panel_notas" style="display:" class="row">
                    <div class="form-group form-custom hidden-xs">
                        <label class="control-label col-sm-1 nombredato" style="text-align:left !important;"><b>Notas&nbsp;</b></label>
                        <div class="col-sm-11 cssdefault">
                            <textarea id="notas" name ="notas" rows="4" cols="150"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Guardar</button>
        &nbsp;
        <button class="btn btn-primary" type="reset">Borrar</button>
        &nbsp;
        <button class="btn btn-primary" type="button" onclick="location.href = '{{url('/clientes/show')}}/{{$cliente}}'">Volver</button>
        <script>
            $('#fecha_publicacion').datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    yearRange: '-115:+15'
            });
            //$('#fecha_publicacion').datepicker("option", $.datepicker.regional['fr']);
            //$('#fecha_publicacion').attr("tabindex",-1);	
        </script>
    </form>
    @stop
