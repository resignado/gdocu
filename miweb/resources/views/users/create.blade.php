@extends('layouts.master')
    @section('content')
    <form action="{{ url('user/save') }}" >
        {{ csrf_field() }}
        <input type="hidden" name="rol" id="rol" value="{{$rol}}">
        @if(isset($id_cliente))
        <input type="hidden" name="id_cliente" id="id_cliente" value="{{$id_cliente}}">
        @endif
        <div class="portlet light bordered portlet-custom clearfix">
            <div class="portlet-title">
                <div class="caption" style="cursor:pointer;">
                    <i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos Usuario				
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display:block;">
                <div id="panel_datos_personales" style="display:" class="row">
                    <div class="col-sm-12 form-horizontal">
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Email&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="email" id="email" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Password&nbsp;</b></label>
                            <div class="valor_elemento col-sm-8">
                                <input class="strong-password form-control input-sm" name="password" id="password" value="" type="password">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="nombre" id="nombre" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Apellidos</b></label>
                            <div class="col-sm-8 ">
                                <input name="apellidos" id="apellidos" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Tel&eacute;fono&nbsp;</b></label>
                            <div class="col-sm-8 csstelefono">
                                <div class="input-icon input-icon-sm">
                                    <i class="fa fa-phone"></i>
                                    <input name="telefono" id="telefono" class="form-control input-sm" maxlength="" type="text">
                                </div>
                            </div>    
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>M&oacute;vil&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <div class="input-icon input-icon-sm">
                                    <i class="fa fa-phone"></i>
                                    <input name="movil" id="movil" class="form-control input-sm" maxlength="" type="text">
                                </div>
                            </div>  
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Direcci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="direccion" id="direccion" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        @if($rol != 2)
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Administrador</b></label>
                            <div class="col-sm-8 ">
                                <input name="administrador" id="administrador" type="checkbox">
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Guardar</button>
        &nbsp;
        <button class="btn btn-primary" type="reset">Borrar</button>
        &nbsp;
        <button class="btn btn-primary" type="button" onclick="location.href = '{{url('/user/list')}}'">Volver</button>
    </form>
    @stop
