@extends('layouts.master')
    @section('content')
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="caption-subject font-purple-soft bold uppercase">Usuarios</span>
                </div>
            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" data-toggle="tab" aria-expanded="true"> Usuarios </a>
                    </li>
                    <li class="">
                        <a href="#tab_1_2" data-toggle="tab" aria-expanded="false"> Usuarios Online </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_1_1">
                        <a class="btn btn-primary" href="{{url('/user/add')}}/rol/1" style="float:right !important">
                            <span>&#10010</span> Alta Usuario
                        </a>
                        <br/>
                        <table class="table table-striped table-condensed table-hover">
                            <thead>
                                <tr>	
                                    <th>&nbsp;</th>
                                    <th class=" pointer" > <span style="cursor:pointer">Usuario</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Nombre</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Apellidos</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Tel&eacute;fono</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">M&oacute;vil</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Email</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Es administrador</span></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                             @foreach( $usuarios as $key => $usuario )                            
                             <tr id="fila_user_{{$usuario['id']}}">
                                <td class="cssdefault"><a href="{{ url('/user/edit/' . $usuario['id'] ) }}"><i class="fa fa-edit"></i></a></td>
                                <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['email']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['nombre']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['apellidos']}}&nbsp;</a></td>
                                <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['telefono']}}&nbsp;</a></td>
                                <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['movil']}}&nbsp;</a></td>
                                <td class="cssemail"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['email']}}&nbsp;</a></td>
                                <td class="cssemail"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">
                                       @if($usuario['es_administrador'] == 1)
                                            <i class="fa fa-check green"></i>
                                        @else
                                            <i class="fa fa-close red"></i>
                                        @endif                                    
                                       </a>
                                   </td>
                                <td class="cssdefault"><a href="{{ url('/user/delete/' . $usuario['id'] ) }}"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab_1_2">
                        <table class="table table-striped table-condensed table-hover">
                            <thead>
                                <tr>	
                                    <th>&nbsp;</th>
                                    <th class=" pointer" > <span style="cursor:pointer">Usuario</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Nombre</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Apellidos</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Tel&eacute;fono</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">M&oacute;vil</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Email</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Es administrador</span></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach( $usuarios_online as $key => $usuario_online )                            
                                <tr id="fila_user_{{$usuario_online['id']}}">
                                   <td class="cssdefault"><a href="{{ url('/user/edit/' . $usuario_online['id'] ) }}"><i class="fa fa-edit"></i></a></td>
                                   <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">{{$usuario_online['email']}}&nbsp;</a></td>
                                   <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario_online['nombre']}}&nbsp;</a></td>
                                   <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">{{$usuario_online['apellidos']}}&nbsp;</a></td>
                                   <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">{{$usuario_online['telefono']}}&nbsp;</a></td>
                                   <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">{{$usuario_online['movil']}}&nbsp;</a></td>
                                   <td class="cssemail"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">{{$usuario_online['email']}}&nbsp;</a></td>
                                   <td class="cssemail"><a href="{{ url('/user/show/' . $usuario_online['id'] ) }}" style="text-decoration:none;">
                                       @if($usuario_online['es_administrador'] == 1)
                                            <i class="fa fa-check green"></i>
                                        @else
                                            <i class="fa fa-close red"></i>
                                        @endif                                    
                                       </a>
                                   </td>
                                   <td class="cssdefault"><a href="{{ url('/user/delete/' . $usuario_online['id'] ) }}"><i class="fa fa-trash"></i></a></td>                                   
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @stop
    
    

