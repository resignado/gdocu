@extends('layouts.master')
    @section('content')
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos del Usuario				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_datos_personales" style="display:" class="row">
		<div class="col-sm-6 form-horizontal">
                    <div class="col-sm-12 form-horizontal">
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Email&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_email">{{$datosusuario['email']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_nombre">{{$datosusuario['nombre']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Apellidos&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_apellidos">{{$datosusuario['apellidos']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Tel&eacute;fono&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <span class="form-control-static" id="view_telefono">{{$datosusuario['telefono']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>M&oacute;vil&nbsp;</b></label>
                            <div class="col-sm-8 csstexto_salto">
                                <span class="form-control-static" id="view_cp">{{$datosusuario['movil']}}</span>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Direcci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 csstexto_salto">
                                <span class="form-control-static" id="view_direccion">{{$datosusuario['direccion']}}</span>
                            </div>
                        </div>
                        @if($rol != 2)
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Administrador&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                @if($datosusuario['es_administrador'] == 1)
                                    <i class="fa fa-check green"></i>
                                @else
                                    <i class="fa fa-close red"></i>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
		</div>
            </div>
	</div>
    </div>
    @stop
    
    
