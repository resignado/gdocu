@extends('layouts.master')
    @section('content')
        <div id="contenido_list_clientes_propios">    
            <div class="table-responsive">
		<table class="table table-striped table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th class=" pointer" > <span style="cursor:pointer">Nombre</span></th>
                            <th class=" pointer" > <span style="cursor:pointer">Apellidos</span></th>
                            <th class=" pointer" > <span style="cursor:pointer">Nif cif</span></th>
                            <th class=" pointer" > <span style="cursor:pointer">Poblacion</span></th>
                            <th class=" pointer" > <span style="cursor:pointer">Telefono</span></th>
                            <th class=" pointer" > <span style="cursor:pointer">Email</span></th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $clientes as $key => $cliente )
                            <tr id="fila_clientes_{{$cliente['id']}}">
                                <td class="cssdefault"><a href="{{ url('/clientes/edit/' . $cliente['id'] ) }}"><i class="fa fa-edit"></i></a></td>
                                <td class="csstexto_salto"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['nombre']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['apellidos']}}&nbsp;</a></td>
                                <td class="cssdni"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['nif']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['poblacion']}}&nbsp;</a></td>
                                <td class="csstelefono"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['telefono']}}&nbsp;</a></td>
                                <td class="cssemail"><a href="{{ url('/clientes/show/' . $cliente['id'] ) }}" style="text-decoration:none;">{{$cliente['email']}}&nbsp;</a></td>
                                <td class="cssdefault"><a href="{{ url('/clientes/delete/' . $cliente['id'] ) }}"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>           
            </div>
        </div>
    @stop
    
    

