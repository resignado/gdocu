@extends('layouts.master')
    @section('content')
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>Documentos&nbsp;				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_documentos" style="display:" class="row">
                <div id="contenido_botones_list_der_gdocu" class="page-toolbar pull-right">
                    <button type="button" id="" class="btn btn-primary btn-custom btn-sm " onclick="location.href = '{{url('/documentos/add/cliente/')}}/{{$datoscliente['id']}}'" aria-expanded="false">
                        <i class="fa fa-paperclip"></i>&nbsp;Adjuntar Documento/s				
                    </button>
                </div>
                <br/><br/>
                <div id="contenido_gdocu_clientes">
                    @if( count($documentos) == 0)                    
                    <br/><br/><p class="info-sin-registros text-center margin-bottom-20">No hay documentos asociados a este cliente</p>
                    @elseif (count($documentos) > 0)
                    <div class="table-responsive col-sm-12">
                    <table class="table table-striped table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class=" pointer" > <span style="cursor:pointer">Documento</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Fecha publicación</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Online</span></th>
                                    <th class=""> <span style="cursor:pointer">Descargar</span></th>
                                </tr>
                            </thead>
                            @foreach( $documentos as $key => $documento )
                            <tr id="fila_clientes_{{$documento['id']}}">
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;">{{$documento['nombre']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;"><?=date('Y-m-d',strtotime($documento['fecha_publicacion']))?>&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;">
                                        @if($documento['online'] == 1)
                                            <i class="fa fa-check green"></i>
                                        @else
                                            <i class="fa fa-close red"></i>
                                        @endif
                                    </a>
                                </td>
                                <td class="csstexto_salto"><a href="{{ url('documentos/descargaficheros3/' . $documento['id']) }}" target="_blank"><i class="fa fa-download"></i></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
		</div>
                
            </div>
	</div>
    </div>
    @stop
    
    
