@extends('layouts.master')
    @section('content')
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos Personales				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_datos_personales" style="display:" class="row">
		<div class="col-sm-6 form-horizontal">
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_nombre">{{$datoscliente['nombre']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nif-Cif&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_nif">{{$datoscliente['nif']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Direcci&oacute;n&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_direccion">{{$datoscliente['direccion']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Poblaci&oacute;n&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_direccion">{{$datoscliente['poblacion']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Tel&eacute;fono&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_telefono">{{$datoscliente['telefono']}}</span>
                        </div>
                    </div>
		</div>
		<div class="col-sm-6 form-horizontal">
                    <div class="form-group form-custom hidden-xs">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Apellidos&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_apellidos">{{$datoscliente['apellidos']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Email&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_email">{{$datoscliente['email']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Cod. Postal&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_cp">{{$datoscliente['codigopostal']}}</span>
                        </div>
                    </div>
                    <div class="form-group form-custom ">
                        <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>M&oacute;vil&nbsp;</b></label>
                        <div class="col-sm-8 csstexto_salto">
                            <span class="form-control-static" id="view_movil">{{$datoscliente['movil']}}</span>
                        </div>
                    </div>
		</div>
            </div>
	</div>
    </div>
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>Usuario Online&nbsp;				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_notas" style="display:" class="row">
		<div class="form-group form-custom hidden-xs">
                    @if( count($usuario_online) == 0)      
                    <div id="contenido_botones_list_der_usercu" class="page-toolbar pull-right">
                        <button type="button" id="" class="btn btn-primary btn-custom btn-sm " onclick="location.href = '{{url('/user/add/')}}/rol/2/cliente/{{$datoscliente['id']}}'" aria-expanded="false">
                            <i class="glyphicon glyphicon-user"></i>&nbsp;Alta Usuario Online				
                        </button>
                    </div>    
                    <br/><br/><p class="info-sin-registros text-center margin-bottom-20">No hay usuario online asociados a este cliente</p>
                    @elseif (count($usuario_online) > 0)
                    <br/><br/>
                    <table class="table table-striped table-condensed table-hover">
                        <thead>
                            <tr>	
                                <th>&nbsp;</th>
                                <th class=" pointer" > <span style="cursor:pointer">Usuario</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">Nombre</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">Apellidos</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">Tel&eacute;fono</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">M&oacute;vil</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">Email</span></th>
                                <th class=" pointer" > <span style="cursor:pointer">Es administrador</span></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                         @foreach($usuario_online as $key => $usuario )                            
                         <tr id="fila_user_{{$usuario['id']}}">
                            <td class="cssdefault"><a href="{{ url('/user/edit/' . $usuario['id'] ) }}/cliente/{{$datoscliente['id']}}"><i class="fa fa-edit"></i></a></td>
                            <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['email']}}&nbsp;</a></td>
                            <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['nombre']}}&nbsp;</a></td>
                            <td class="csstexto_salto"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['apellidos']}}&nbsp;</a></td>
                            <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['telefono']}}&nbsp;</a></td>
                            <td class="csstelefono"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['movil']}}&nbsp;</a></td>
                            <td class="cssemail"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">{{$usuario['email']}}&nbsp;</a></td>
                            <td class="cssemail"><a href="{{ url('/user/show/' . $usuario['id'] ) }}" style="text-decoration:none;">
                                   @if($usuario['es_administrador'] == 1)
                                        <i class="fa fa-check green"></i>
                                    @else
                                        <i class="fa fa-close red"></i>
                                    @endif                                    
                                   </a>
                               </td>
                            <td class="cssdefault"><a href="{{ url('/user/delete/' . $usuario['id'] ) }}/cliente/{{$datoscliente['id']}}"><i class="fa fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
	</div>
    </div>
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>Notas&nbsp;		
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_notas" style="display:" class="row">
		<div class="form-group form-custom hidden-xs">
                    <label class="control-label col-sm-1 nombredato" style="text-align:left !important;"><b>Notas&nbsp;</b></label>
                    <div class="col-sm-11 cssdefault">
                        <span class="form-control-static" id="view_notas">{{$datoscliente['notas']}}</span>
                    </div>
                </div>
            </div>
	</div>
    </div>
    <div class="portlet light bordered portlet-custom clearfix">
        <div class="portlet-title">
            <div class="caption" style="cursor:pointer;">
		<i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>Documentos&nbsp;				
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
	</div>
        <div class="portlet-body" style="display:block;">
            <div id="panel_documentos" style="display:" class="row">
                <div id="contenido_botones_list_der_gdocu" class="page-toolbar pull-right">
                    <button type="button" id="" class="btn btn-primary btn-custom btn-sm " onclick="location.href = '{{url('/documentos/add/cliente/')}}/{{$datoscliente['id']}}'" aria-expanded="false">
                        <i class="fa fa-paperclip"></i>&nbsp;Adjuntar Documento/s				
                    </button>
                </div>
                <br/><br/>
                <div id="contenido_gdocu_clientes">
                    @if( count($documentos) == 0)                    
                    <br/><br/><p class="info-sin-registros text-center margin-bottom-20">No hay documentos asociados a este cliente</p>
                    @elseif (count($documentos) > 0)
                    <div class="table-responsive col-sm-12">
                    <table class="table table-striped table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class=" pointer" > <span style="cursor:pointer">Documento</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Fecha publicación</span></th>
                                    <th class=" pointer" > <span style="cursor:pointer">Online</span></th>
                                    <th class=""> <span style="cursor:pointer">Descargar</span></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            @foreach( $documentos as $key => $documento )
                            <tr id="fila_clientes_{{$documento['id']}}">
                                <td class="cssdefault"><a href="{{ url('/documentos/edit/' . $documento['id'] .'/cliente/' . $datoscliente['id']) }}"><i class="fa fa-edit"></i></a></td>
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;">{{$documento['nombre']}}&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;"><?=date('Y-m-d',strtotime($documento['fecha_publicacion']))?>&nbsp;</a></td>
                                <td class="csstexto_salto"><a href="{{ url('/documentos/show/' . $documento['id'] ) }}" style="text-decoration:none;">
                                        @if($documento['online'] == 1)
                                            <i class="fa fa-check green"></i>
                                        @else
                                            <i class="fa fa-close red"></i>
                                        @endif
                                    </a>
                                </td>
                                <td class="csstexto_salto"><a href="{{ url('documentos/descargaficheros3/' . $documento['id']) }}" target="_blank"><i class="fa fa-download"></i></a></td>
                                <!--<td class="csstexto_salto"><a href="javascript:downloadDocumento_s3('<?=$documento['id']?>')"><i class="fa fa-download"></i></a>-->
                                <td class="cssdefault"><a href="{{ url('/documentos/delete/' . $documento['id'] ) }}"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
		</div>
                
            </div>
	</div>
    </div>
    @stop
    
    
