@extends('layouts.master')
    @section('content')
    <form action="{{ url('clientes/save') }}" method="POST">
        {{ csrf_field() }}
        <div class="portlet light bordered portlet-custom clearfix">
            <div class="portlet-title">
                <div class="caption" style="cursor:pointer;">
                    <i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Datos Personales				
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display:block;">
                <div id="panel_datos_personales" style="display:" class="row">
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nombre&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="nombre" id="nombre" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Nif-Cif&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="nif" id="nif" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Direcci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="direccion" id="direccion" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Poblaci&oacute;n&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="poblacion" id="poblacion" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Tel&eacute;fono&nbsp;</b></label>
                            <div class="col-sm-8 csstelefono">
                                <div class="input-icon input-icon-sm">
                                    <i class="fa fa-phone"></i>
                                    <input name="telefono" id="telefono" class="form-control input-sm" maxlength="" type="text">
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="col-sm-6 form-horizontal">
                        <div class="form-group form-custom hidden-xs">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Apellidos&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="apellidos" id="apellidos" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Email&nbsp;</b></label>
                            <div class="col-sm-8 cssemail">
                                <div class="input-icon input-icon-sm">
                                    <i class="fa fa-envelope-o"></i>
                                    <input name="email" id="email" class="form-control" maxlength="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>Cod. Postal&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <input name="codigopostal" id="codigopostal" class="form-control input-sm" maxlength="" type="text">
                            </div>
                        </div>
                        <div class="form-group form-custom ">
                            <label class="control-label col-sm-4 nombredato" style="text-align:left !important;"><b>M&oacute;vil&nbsp;</b></label>
                            <div class="col-sm-8 ">
                                <div class="input-icon input-icon-sm">
                                    <i class="fa fa-phone"></i>
                                    <input name="movil" id="movil" class="form-control input-sm" maxlength="" type="text">
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet light bordered portlet-custom clearfix">
            <div class="portlet-title">
                <div class="caption" style="cursor:pointer;">
                    <i id="icon-share font-blue-crusta" class="icon-arrow-down-up"></i>&nbsp;Notas				
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display:block;">
                <div id="panel_notas" style="display:" class="row">
                    <div class="form-group form-custom hidden-xs">
                        <label class="control-label col-sm-1 nombredato" style="text-align:left !important;"><b>Notas&nbsp;</b></label>
                        <div class="col-sm-11 cssdefault">
                            <textarea id="notas" name ="notas" rows="4" cols="150"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Guardar</button>
        &nbsp;
        <button class="btn btn-primary" type="reset">Borrar</button>
        &nbsp;
        <button class="btn btn-primary" type="button" onclick="location.href = '{{url('/clientes')}}'">Volver</button>
    </form>
    @stop
