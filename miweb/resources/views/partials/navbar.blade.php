<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <!--
        <a class="navbar-brand" href="{{url('/')}}" style="color:#777"><i class="fas fa-file"></i>&nbsp;Gdocu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        -->

        @if( true || Auth::check() )
            <div class="collapse navbar-collapse" id="navbarSupportedContent">    
                @if(Auth::user()->rol == 1)
                <a class="btn btn-primary" href="{{url('/clientes')}}">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    Listado
                </a>
                &nbsp;
                @endif
                
                @if(Auth::user()->rol == 2)
                <a class="btn btn-primary" href="{{url('clientes/showdocumentos/cliente/'.Auth::user()->id_cliente)}}">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    Listado Docs
                </a>
                &nbsp;
                @endif
                
                @if(Auth::user()->rol == 1 && Auth::user()->es_administrador == 1)
                <a class="btn btn-primary" href="{{url('/user/list')}}" >
                    <span class="glyphicon glyphicon-user"></span>
                    Usuarios
                </a>
                @endif
                
                @if(Request::is('clientes') OR Request::is('/'))
                <a class="btn btn-primary" href="{{url('/clientes/create')}}">
                    <span>&#10010</span> Nuevo cliente
                </a>
                @endif
                
                @if(Request::is('clientes/show/*'))
                <a class="btn btn-primary" href="{{url('/clientes/edit')}}/{{$datoscliente['id']}}">
                    <span>&#10010</span> Editar cliente
                </a>
                @endif
                
                <form action="{{ url('/logout') }}" method="POST" style="display:inline;float:right !important">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary" style="display:inline;cursor:pointer;">
                        <span class="glyphicon glyphicon-log-out"></span>
                        Cerrar sesión
                    </button>
                </form>
            </div>
        @endif
    </div>
</nav>
