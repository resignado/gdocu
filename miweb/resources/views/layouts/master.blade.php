<html>
    <head>
        {{-- Latest compiled and minified CSS --}}
        <title>Gestor Documental</title>
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
        {{-- Latest compiled and minified JavaScript --}}
        <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>-->
        <link href="{{ url('/metronic/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"  rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/typeahead/typeahead.css') }}" rel="stylesheet'" type="text/css" />
        <link href="{{ url('metronic/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('/metronic/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/layouts/layout/css/themes/light.min.css') }}" rel="stylesheet" id="style_color" type="text/css" />
        <link href="{{ url('/metronic/layouts/layout/css/custom.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ url('/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script type="text/javascript" src="{{ url('/js/funciones.js') }}"></script>
        <script type="text/javascript" src="{{ url('/jquery/jquery-1.12.4.min.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="{{ url('/jquery/jquery-migrate-1.2.1.js') }}"></script>
        <script type="text/javascript" src="{{ url('/jquery/jquery-ui-1.12.min.js') }}"></script>        
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/js.cookie.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/jquery.blockui.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/autosize/autosize.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-toastr/toastr.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/pages/scripts/ui-toastr.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/typeahead/typeahead.bundle.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/typeahead/handlebars.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/pages/scripts/ui-blockui.min.js') }}"></script> 
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/jquery.blockui.min.js') }}"></script> 
        <script type="text/javascript" src="{{ url('/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/global/scripts/app.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/layouts/layout/scripts/layout.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/layouts/layout/scripts/demo.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/layouts/layout/scripts/layout.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/layouts/global/scripts/quick-sidebar.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/layouts/global/scripts/quick-nav.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('/metronic/pages/scripts/portlet-draggable.min.js') }}"></script>        
    </head>
    <body>
        <h1>Hola {{ Auth::user()->nombre }}, bienvenido al gestor documental</h1>
        @include('partials.navbar')
        <div class="container">
            @yield('content')
        </div>
    </body>
    
   
</html>
