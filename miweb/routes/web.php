<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*NOTAS DEL PROGRAMADOR
    OJO CON LAS VISTA NO DEBEN LLEVAR NUMEROS
 */
/*Route::get('/', function () {
    return view('welcome');
});*/

//PARA PROBAR EL HOMECONTROLLER LO QUITAMOS
/*Route::get('/', function()
{
    return view('home');
});*/

Auth::routes();

Route::get('/', function()
{   if (Auth::user()->rol == 1){
        //Rol administrador o usuario
        return redirect('clientes');
    } else {
        //Rol usuario online
        return redirect('clientes/showdocumentos/cliente/'.Auth::user()->id_cliente);
    }     
})->middleware('auth');

//Route::get('/', 'ClientesController@getIndex')->name('home');

//Cuando funcionamos sin el auth
/*
 
Route::get('/', 'IndexController@getHome');

Route::get('login', function()
{
    return view('auth.login');
});

Route::get('logout', function()
{
    return "logout de usuario";
});
*/

//////////////////////////////////CLIENTES
Route::get('clientes', 'ClientesController@getIndex')->middleware('auth');

Route::get('clientes/show/{id}', 'ClientesController@getShow')->middleware('auth');

Route::get('clientes/create/', 'ClientesController@getCreate')->middleware('auth');

Route::get('clientes/edit/{id}', 'ClientesController@getEdit')->middleware('auth');

Route::get('clientes/delete/{id}', 'ClientesController@getDelete')->middleware('auth');

Route::get('clientes/showdocumentos/cliente/{id}', 'ClientesController@getDocumentoscliente')->middleware('auth');

Route::match(array('GET', 'POST'),'clientes/save/', 'ClientesController@saveCliente')->middleware('auth');

Route::match(array('GET', 'POST'),'clientes/saveedit/', 'ClientesController@saveeditCliente')->middleware('auth');

//////////////////////////////////DOCUMENTOS

Route::get('documentos/add/cliente/{id}', 'DocumentosController@getCreate')->middleware('auth');

Route::get('documentos/delete/{id}', 'DocumentosController@getDelete')->middleware('auth');

Route::get('documentos/edit/{id}/cliente/{id_cliente}', 'DocumentosController@getEdit')->middleware('auth');

Route::get('documentos/show/{id}', 'DocumentosController@getShow')->middleware('auth');

Route::match(array('GET', 'POST'),'documentos/save/', 'DocumentosController@saveDocumento')->middleware('auth');

Route::match(array('GET', 'POST'),'documentos/saveedit/', 'DocumentosController@saveeditDocumento')->middleware('auth');

Route::match(array('GET', 'POST'),'documentos/descargaficheros3/{id}', 'DocumentosController@descargaficheros3')->middleware('auth');

//////////////////////////////////USUARIOS

Route::get('user/list/', 'UserController@getList')->middleware('auth');

Route::get('user/add/rol/{id}', 'UserController@getCreate')->middleware('auth');

Route::get('user/add/rol/{id}/cliente/{id_cliente}', 'UserController@getCreate')->middleware('auth');

Route::get('user/show/{id}', 'UserController@getShow')->middleware('auth');

Route::get('user/edit/{id}', 'UserController@getEdit')->middleware('auth');

Route::get('user/edit/{id}/cliente/{id_cliente}', 'UserController@getEdit')->middleware('auth');

Route::get('user/delete/{id}', 'UserController@getDelete')->middleware('auth');

Route::get('user/delete/{id}/cliente/{id_cliente}', 'UserController@getDelete')->middleware('auth');

Route::match(array('GET', 'POST'),'user/save/', 'UserController@saveUser')->middleware('auth');

Route::match(array('GET', 'POST'),'user/saveedit/', 'UserController@saveeditUser')->middleware('auth');
/*
Route::get('/', 'HomeController@getHome');

Route::get('clientes', function()
{
    return view('clientes.index');
});

Route::get('clientes/show/{id}', function($id = 0)
{
    return view('clientes.show', array('id'=>$id));
});

Route::get('clientes/create', function()
{
    return view('clientes.create');
});

Route::get('clientes/edit/{id}', function($id = 0)
{
    return view('clientes.edit', array('id'=>$id));
});

Route::get('user/{id}', 'UserController@showProfile');

//Route::controller('users', 'UserController');

Route::get('dashboard', ['middleware' => 'es_mayor_de_edad', function () {
//...
}]);
*/
//Para pasar un parámetro a un middleware en la definición de una ruta lo tendremos que
//añadir a continuación del nombre del filtro separado por dos puntos, por ejemplo:
/*
Route::put('post/{id}', ['middleware' => 'role:editor', function ($id) {
    //
}]);
*/
//Esta opción es muy útil para aplicar un filtro sobre todo un conjunto de rutas, de esta forma
//solo tendremos que especificar el filtro una vez y además nos permitirá dividir las rutas en
//secciones (distinguiendo mejor a que secciones se les está aplicando un filtro):
/*
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
    // Ruta filtrada por el middleware
    });
    Route::get('user/profile', function () {
    // Ruta filtrada por el middleware
    });
});
*/

//También podemos utilizar la opción de agrupar rutas para indicar un prefijo que se añadirá a
//todas las URL del grupo. Por ejemplo, si queremos definir una sección de rutas que
//empiecen por el prefijo dashboard tendríamos que hacer lo siguiente:
/*
Route::group(['prefix' => 'dashboard'], function () {
    Route::get('catalog', function () {  });
    Route::get('users', function () {  });
});
*/

//REDIRECCIONES
//return redirect('user/login');

//O si queremos volver a la ruta anterior simplemente podemos usar el método back :
//return back();

//A traves de controlador
//return redirect()->action('HomeController@index');
//Igual que el anterior pero con parametros
//return redirect()->action('UserController@profile', [1]);

//Devolver con parametros del formulario
//return redirect('form')->withInput();
// O para reenviar los datos de entrada excepto algunos:
//return redirect('form')->withInput($request->except('password'));

//Este método también lo podemos usar con la función back o con la función action :
//return back()->withInput();
//return redirect()->action('HomeController@index')->withInput();

///////////////////////////////////////////PRUEBAS DEL MANUAL///////////////////////////////////////

//SOLO POST
/*Route::post('foo/bar', function()
{
    return '¡Hola mundo por post!';
});
 */

//POST Y GET A LA VEZ
/*Route::match(array('GET', 'POST'), '/', function()
{
return '¡Hola mundo!';
});
 */

//CUALQUIER TIPO DE PETICION
/*Route::any('foo/bar', function()
{
    return '¡Hola mundo por any!';
});*/

//Variable obligatoria o da error
/*Route::get('user/{id}', function($id)
{
    return 'Usuario '.$id;
});
*/
//Variable opcional
/*
Route::get('username/{name?}', function($name = null)
{
    if ($name){
        return "El usuario ".$name;
    } else {
        return "No se ha especificado el nombre del usuario";
    }    
});
*/
//Igual que el anterior pero si no viene, ponemos por defecto
/*
Route::get('username/{name?}', function($name = 'root')
{
    return "El usuario ".$name;
});
*/
//Lo mismo pero con expresiones regulares
/*Route::get('username/{name}', function($name)
{
    echo "username con letras";
})
->where('name', '[A-Za-z]+');
Route::get('userid/{id}', function($id)
{
    echo "userid con numeros";
})
->where('id', '[0-9]+');
*/
// Si hay varios parámetros podemos validarlos usando un array:
/*
Route::get('user/{id}/{name}', function($id, $name)
{
//
})
->where(array('id' => '[0-9]+', 'name' => '[A-Za-z]+'));
 */
/*
Route::any('/devuelveruta/{ruta?}', function($ruta = '')
{
    return url($ruta);
});

Route::get('/nombre/{nombre?}', function($nombre='')
{
    if (!$nombre){
        $nombre = 'root';
    }
    return view('home', array('nombre' =>$nombre));
});
/*
Route::get('/pruebablade/{nombre?}', function($nombre = '')
{
    if (!$nombre){
        $nombre = 'Sin Nombre';
    }
    return view('prueba',array('nombre' =>$nombre));
});

Route::get('index', function(){
    return view('index');
});
*/


//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
